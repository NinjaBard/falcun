#include "src/stdafx.h"
#include "HapticsClass.h"
#include <Windows.h>
#include <math.h>

HDLServoOpExitCode ContactCallback(void* pUserData) { 
	HapticsClass* haptics = static_cast<HapticsClass*>(pUserData);

	hdlToolPosition(haptics->m_devicePos);
	hdlToolButtons(&haptics->activeButtonsMask);
	
	haptics->Update();

	hdlSetToolForce(haptics->m_deviceForce);

	return HDL_SERVOOP_CONTINUE;
}

HDLServoOpExitCode GetStateCallback(void* pUserData) {
	HapticsClass* haptics = static_cast<HapticsClass*>(pUserData);

	haptics->Sync();

	return HDL_SERVOOP_EXIT;
}

HapticsClass::HapticsClass() : m_deviceHandle(HDL_INVALID_HANDLE), m_servoOp(HDL_INVALID_HANDLE), m_isInited(false) {
	for (int i = 0; i < 3; i++) {
		m_devicePos[i] = 0;
		m_deviceForce[i] = 0;
	}
}

HapticsClass::~HapticsClass() {
	Uninit();
}

void HapticsClass::Init(double gameWorkspace[6]) {
	HDLError err = HDL_NO_ERROR;

	m_deviceHandle = hdlInitNamedDevice("DEFAULT");
	TestHDLError("hdlInitDevice");

	if (m_deviceHandle == HDL_INVALID_HANDLE) {
		MessageBoxA(NULL, "Could not open device", "Device Failure", MB_OK);
		exit(0);
	}

	hdlStart();
	TestHDLError("hdlStart");

	m_servoOp = hdlCreateServoOp(ContactCallback, this, false);
	if (m_servoOp == HDL_INVALID_HANDLE) {
		MessageBoxA(NULL, "Invalid servo op handle", "Device Failure", MB_OK);
	}
	TestHDLError("hdlCreateServoOp");

	hdlMakeCurrent(m_deviceHandle);
	TestHDLError("hdlMakeCurrent");

	hdlDeviceWorkspace(m_workspaceDims);
	TestHDLError("hdlDeviceWorkspace");

	bool useUniformScale = true;
	hdluGenerateHapticToAppWorkspaceTransform(m_workspaceDims,
											  gameWorkspace,
											  useUniformScale,
											  m_transformMat);
	TestHDLError("hdluGenerateHapticToAppWorkspaceTransform");

	m_isInited = true;
}

void HapticsClass::Uninit() {
	if (m_servoOp != HDL_INVALID_HANDLE) {
		hdlDestroyServoOp(m_servoOp);
		m_servoOp = HDL_INVALID_HANDLE;
	}

	hdlStop();
	if (m_deviceHandle != HDL_INVALID_HANDLE) {
		hdlUninitDevice(m_deviceHandle);
		m_deviceHandle = HDL_INVALID_HANDLE;
	}

	m_isInited = false;
}

void HapticsClass::TestHDLError(const char* str) {
	HDLError err = hdlGetError();
	if (err != HDL_NO_ERROR) {
		MessageBoxA(NULL, str, "HDAL_ERROR", MB_OK);
		abort();
	}
}

void HapticsClass::SyncFromServo() {
	hdlCreateServoOp(GetStateCallback, this, true);
}

void HapticsClass::Sync() {
	
}

void HapticsClass::VecMultMatrix(double srcVec[3], double mat[16], double dstVec[3]) {
	dstVec[0] = mat[0] * srcVec[0]
		+ mat[4] * srcVec[1]
		+ mat[8] * srcVec[2]
		+ mat[12];

	dstVec[1] = mat[1] * srcVec[0]
		+ mat[5] * srcVec[1]
		+ mat[9] * srcVec[2]
		+ mat[13];

	dstVec[2] = mat[2] * srcVec[0]
		+ mat[6] * srcVec[1]
		+ mat[10] * srcVec[2]
		+ mat[14];
}

void HapticsClass::Update() {
	VecMultMatrix(m_devicePos, m_transformMat, m_appPosition);
}

void HapticsClass::GetPosition(double pos[3]) {
	pos[0] = m_appPosition[0];
	pos[1] = m_appPosition[1];
	pos[2] = m_appPosition[2];
}

void HapticsClass::GetRawPosition(double pos[3]) {
	pos[0] = m_devicePos[0];
	pos[1] = m_devicePos[1];
	pos[2] = m_devicePos[2];
}

void HapticsClass::GetActiveForce(double force[3]) {
	force[0] = m_deviceForce[0];
	force[1] = m_deviceForce[1];
	force[2] = m_deviceForce[2];
}

void HapticsClass::SetForce(double force[3]) {
	m_deviceForce[0] = force[0];
	m_deviceForce[1] = force[1];
	m_deviceForce[2] = force[2];
}

bool HapticsClass::IsDeviceCalibrated() {
	unsigned int state = hdlGetState();
	return ((state & HDAL_NOT_CALIBRATED) == 0);
}

bool HapticsClass::IsDeviceReady() {
	unsigned int state = hdlGetState();
	return (state == HDAL_ISREADY);
}

bool HapticsClass::IsButtonDown(int button) {
	if (button < 4 && (activeButtonsMask & (1 << button))) {
		return true;
	}

	return false;
}