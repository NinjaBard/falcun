#ifndef WRAPPER_H
#define WRAPPER_H

#define EXT extern "C"
#define DLLFUNC extern "C" __declspec(dllexport)

#ifdef DLL_USE
#define F(f) (*f)
#else
#define F(f) f
#endif

#undef F
#undef EXT

typedef struct LINK {
	long index;
	struct LINK *next;
};

typedef struct STRING {
	char *chars;
	long length;
};

inline long _LONG(int i) { return i << 10; }					// int -> var
inline long _LONG(double f) { return (long)(f*(1 << 10)); }		// double -> var, overloaded
inline int _INT(long x) { return x >> 10; }					// var -> int
inline float _FLOAT(long x) { return ((float)x) / (1 << 10); }	// var -> float
inline char* _CHR(STRING* s) { return s->chars; }			// STRING* -> char*

#endif