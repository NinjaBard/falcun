#ifndef HAPTICS_CLASS_H
#define HAPTICS_CLASS_H

#include <hdl/hdl.h>
#include <hdlu/hdlu.h>

class HapticsClass {
	friend HDLServoOpExitCode ContactCallback(void *data);
	friend HDLServoOpExitCode GetStateCallback(void *data);

public:
	HapticsClass();

	~HapticsClass();

	void Init(double gameWorkspace[6]);

	void Uninit();

	void Update();

	void GetPosition(double pos[3]);

	void GetRawPosition(double workspacePos[3]);

	void GetActiveForce(double force[3]);

	void SetForce(double force[3]);

	bool IsDeviceCalibrated();

	bool IsDeviceReady();

	bool IsButtonDown(int _button);

	void SyncFromServo();

private:
	void Sync();

	void VecMultMatrix(double srcVec[3], double mat[16], double dstVec[3]);

	void TestHDLError(const char* str);


	bool m_isInited;

	double m_transformMat[16];

	double m_devicePos[3];
	double m_deviceForce[3];

	double m_appPosition[3];
	
	HDLDeviceHandle m_deviceHandle;

	HDLServoOpExitCode m_servoOp;

	double m_workspaceDims[6];

	int activeButtonsMask;
};

#endif