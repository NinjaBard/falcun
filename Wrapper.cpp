#include "src/stdafx.h"
#include "Wrapper.h"

#define DLL_USE
#include "HapticsClass.h"

HapticsClass haptics;

BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved) {
	if (ul_reason_for_call == DLL_PROCESS_DETACH) {
		haptics.Uninit();
	}

	return true;
}

DLLFUNC void StartHaptics(double gameWorkspace[6]) {
	haptics.Init(gameWorkspace);
}

DLLFUNC void StopHaptics() {
	haptics.Uninit();
}

DLLFUNC void SyncHaptics() {
	haptics.SyncFromServo();
}

DLLFUNC double GetXPos() {
	double pos[3];

	haptics.GetPosition(pos);

	return pos[0];
}

DLLFUNC double GetYPos() {
	double pos[3];

	haptics.GetPosition(pos);

	return pos[1];
}

DLLFUNC double GetZPos() {
	double pos[3];

	haptics.GetPosition(pos);

	return pos[2];
}

DLLFUNC double GetRawXPos() {
	double pos[3];

	haptics.GetRawPosition(pos);

	return pos[0];
}

DLLFUNC double GetRawYPos() {
	double pos[3];

	haptics.GetRawPosition(pos);

	return pos[1];
}

DLLFUNC double GetRawZPos() {
	double pos[3];

	haptics.GetRawPosition(pos);

	return pos[2];
}

DLLFUNC double GetForceX() {
	double force[3];

	haptics.GetActiveForce(force);

	return force[0];
}

DLLFUNC double GetForceY() {
	double force[3];

	haptics.GetActiveForce(force);

	return force[1];
}

DLLFUNC double GetForceZ() {
	double force[3];

	haptics.GetActiveForce(force);

	return force[2];
}

DLLFUNC void SetForce(double force[3]) {
	haptics.SetForce(force);
}

DLLFUNC bool IsDeviceCalibrated() {
	return haptics.IsDeviceCalibrated();
}

DLLFUNC bool IsDeviceReady() {
	return haptics.IsDeviceReady();
}

DLLFUNC bool IsButtonDown(int button) {
	return haptics.IsButtonDown(button);
}